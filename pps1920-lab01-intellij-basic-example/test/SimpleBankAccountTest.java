import lab01.example.model.AccountHolder;
import lab01.example.model.BankAccount;
import lab01.example.model.BankAccountWithAtm;
import lab01.example.model.SimpleBankAccount;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

/**
 * The test suite for testing the SimpleBankAccount implementation
 */
class SimpleBankAccountTest {

    private AccountHolder accountHolder;
    private BankAccountWithAtm bankAccount;

    @BeforeEach
    void beforeEach(){
        accountHolder = new AccountHolder("Mario", "Rossi", 1);
        bankAccount = new BankAccountWithAtm(accountHolder, 0);
    }

    @Test
    void testInitialBalance() {
        assertEquals(0, bankAccount.getBalance());
    }

    @Test
    void testDeposit() {
        bankAccount.deposit(accountHolder.getId(), 100);
        assertEquals(100, bankAccount.getBalance());
    }

    @Test
    void testWrongDeposit() {
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.deposit(2, 50);
        assertEquals(100, bankAccount.getBalance());
    }

    @Test
    void testWithdraw() {
        bankAccount.deposit(accountHolder.getId(), 100);
        bankAccount.withdraw(accountHolder.getId(), 70);
        assertEquals(30, bankAccount.getBalance());
    }

    @Test
    void testWrongWithdraw() {
        bankAccount.depositWhitATM(accountHolder.getId(), 100);
        bankAccount.withdrawWhitATM(2, 70);
        assertEquals(100-BankAccountWithAtm.FEE_AMOUNT, bankAccount.getBalance());
    }

    @Test
    void testDepositWithATM() {
        bankAccount.depositWhitATM(accountHolder.getId(), 100);
        assertEquals(100-BankAccountWithAtm.FEE_AMOUNT, bankAccount.getBalance());
    }

    @Test
    void testWrongDepositWithATM() {
        bankAccount.depositWhitATM(accountHolder.getId(), 100);
        bankAccount.depositWhitATM(2, 50);
        assertEquals(100-BankAccountWithAtm.FEE_AMOUNT, bankAccount.getBalance());
    }

    @Test
    void testWithdrawWithATM() {
        bankAccount.depositWhitATM(accountHolder.getId(), 100);
        bankAccount.withdrawWhitATM(accountHolder.getId(), 70);
        assertEquals(30-(BankAccountWithAtm.FEE_AMOUNT*2), bankAccount.getBalance());
    }

    @Test
    void testWrongWithdrawWithATM() {
        bankAccount.depositWhitATM(accountHolder.getId(), 100);
        bankAccount.withdrawWhitATM(2, 70);
        assertEquals(100-BankAccountWithAtm.FEE_AMOUNT, bankAccount.getBalance());
    }

}
