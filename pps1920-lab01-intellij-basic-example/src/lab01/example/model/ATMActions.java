package lab01.example.model;

public interface ATMActions {

    public void setFee(int fee);

    public void depositWhitATM(final int usrID, final double amount);

    public void withdrawWhitATM(final int usrID, final double amount);
}
