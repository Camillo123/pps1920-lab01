package lab01.example.model;

public class BankAccountWithAtm extends SimpleBankAccount implements ATMActions{

    public static final int ST_FEE_AMOUNT = 5;
    private int selectedFee;
    private boolean feeIsSet;
    public BankAccountWithAtm(AccountHolder holder, double balance) {
        super(holder, balance);
        this.selectedFee = -1;
        this.feeIsSet = false;
    }

    @Override
    public void setFee(int fee){
        if(fee > 0){
            this.selectedFee = fee;
            this.feeIsSet = true;
        }

    }

    @Override
    public void depositWhitATM(int usrID, double amount) {
        this.deposit(usrID,amount-(this.feeIsSet ? this.selectedFee : ST_FEE_AMOUNT));
    }

    @Override
    public void withdrawWhitATM(int usrID, double amount) {
        this.withdraw(usrID,amount+(this.feeIsSet ? this.selectedFee : ST_FEE_AMOUNT));
    }
}
