package lab01.example.model;

public abstract class AbstractBankAccount implements BankAccount{

    private double balance;
    private final AccountHolder holder;

    public AbstractBankAccount(final AccountHolder holder, final double balance) {
        this.holder = holder;
        this.balance = balance;
    }

    @Override
    public AccountHolder getHolder(){
        return this.holder;
    }

    @Override
    public double getBalance() {
        return this.balance;
    }


    protected void setBalance(final double amount) {
        this.balance = amount;
    }

    @Override
    public abstract void deposit(final int usrID, final double amount);

    @Override
    public abstract void withdraw(final int usrID, final double amount);

    private boolean isWithdrawAllowed(final double amount){
        return this.balance >= amount;
    }

    private boolean checkUser(final int id) {
        return this.holder.getId() == id;
    }
}
